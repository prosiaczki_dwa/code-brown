import {Component, OnInit} from '@angular/core';
import {Child, GENDER_OPTIONS} from '../child.model';
import {ChildService} from '../child.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-child-add',
  templateUrl: './child-add.component.html',
  styleUrls: ['./child-add.component.css']
})
export class ChildAddComponent implements OnInit {

  genderOptions = GENDER_OPTIONS;
  model = new Child();
  submitted = false;

  constructor(private childService: ChildService, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    this.childService.createChild(this.model).then(r => console.log(r));
    this.router.navigate(['/dashboard']);
  }
}
