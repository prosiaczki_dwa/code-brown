import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ChildService} from "../child.service";
import {Child, GENDER_OPTIONS} from "../child.model";

@Component({
  selector: 'app-child-edit',
  templateUrl: './child-edit.component.html',
  styleUrls: ['./child-edit.component.css']
})
export class ChildEditComponent implements OnInit {

  genderOptions = GENDER_OPTIONS;
  model = new Child();
  submitted = false;

  constructor(private route: ActivatedRoute, private childService: ChildService, private router: Router) { }

  ngOnInit() {
    this.getChild();
  }

  private getChild() {
    const id = this.route.snapshot.paramMap.get('id');
    this.childService.getChild(id).subscribe(snapshot => {
      this.model = {
        id: snapshot.id,
        data: snapshot.data()
      } as Child;
    });
  }

  onSubmit() {
    this.submitted = true;
    this.childService.updateChild(this.model);
    this.router.navigate(['/dashboard']);
  }
}
