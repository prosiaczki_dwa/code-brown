import {Component, OnInit} from '@angular/core';
import {ChildService} from '../child.service';
import {Child} from '../child.model';
import {AuthService} from '../auth.service';
import {MessageService} from '../message.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  children: Child[];

  constructor(private authService: AuthService, private messageService: MessageService, private childService: ChildService, private router: Router) {
  }

  ngOnInit() {
    this.childService.getChildren().subscribe(data => {
      this.children = data.map(e => {
        return {
          id: e.payload.doc.id,
          data: e.payload.doc.data()
        } as Child;
      });
    });
  }

  create(child: Child) {
    this.childService.createChild(child);
  }

  update(child: Child) {
    this.childService.updateChild(child);
  }

  delete(id: string) {
    this.childService.deleteChild(id);
  }

  logout() {
    const userEmail = this.authService.user.email;
    this.authService.logout().then(
      () => {
        const msg = 'User ' + userEmail + ' successfully logged out';
        this.messageService.add(msg);
        this.router.navigate(['/login']);
      }
    )
      .catch(err => {
        this.messageService.add(err.message);
      });
  }

}
