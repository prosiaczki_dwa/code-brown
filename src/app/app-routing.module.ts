import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from './auth.guard';
import {ChildAddComponent} from './child-add/child-add.component';
import {ChildEditComponent} from "./child-edit/child-edit.component";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
    // children: [...]
  },
  {
    path: 'childAdd',
    component: ChildAddComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'childEdit/:id',
    component: ChildEditComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
