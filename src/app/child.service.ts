import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Child} from 'src/app/child.model';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ChildService {

  constructor(private firestore: AngularFirestore, private authService: AuthService) {
  }

  getChildren() {
    const query = ref => ref.where('parentId', '==', this.authService.userId)
    return this.firestore.collection(`children`, query).snapshotChanges();
  }

  createChild(child: Child) {
    child.data.parentId = this.authService.userId;
    return this.firestore.collection('children').add({...child.data});
  }

  updateChild(child: Child) {
    this.firestore.doc('children/' + child.id).update(child.data);
  }

  deleteChild(childId: string) {
    this.firestore.doc('children/' + childId).delete();
  }

  getChild(id: string) {
    return this.firestore.collection('children').doc(id).get();
  }
}
