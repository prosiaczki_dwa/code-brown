export class Child {
  id: string;
  data: ChildData;

  constructor(){
    this.data = new ChildData();
  }
}

class ChildData {
  parentId: string;
  name: string;
  gender: Gender;
  birthDate: Date;
}

enum Gender {
  BOY = 'boy',
  GIRL = 'girl'
}

export const GENDER_OPTIONS = [Gender.BOY, Gender.GIRL];
