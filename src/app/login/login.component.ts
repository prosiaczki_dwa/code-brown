import {Component, OnInit} from '@angular/core';
import {MessageService} from '../message.service';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials = {
    email: '',
    password: ''
  };
  registerInfo = '';

  constructor(private authService: AuthService, private messageService: MessageService, private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.credentials)
      .then(
        () => {
          const msg = 'User logged in ' + this.credentials.email;
          this.messageService.add(msg);
          this.registerInfo = msg;
          this.router.navigate(['/dashboard']);
        }
      )
      .catch(err => {
        this.messageService.add(err.message);
        this.registerInfo = err.message;
      });
  }

  register() {
    this.authService.register(this.credentials)
      .then(() => {
        this.registerInfo = 'ACCOUNT CREATED, PLZ LOGIN IN!';
        this.messageService.add('User registered ' + this.credentials.email);
      })
      .catch(err => {
        this.registerInfo = err.message;
        this.messageService.add(err.message);
      });
  }
}
